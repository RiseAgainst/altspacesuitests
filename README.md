To run test:
gradlew test

To make Lombok work for Intelij Idea:

Preferences (Ctrl + Alt + S)  
Build, Execution, Deployment  
Compiler  
Annotation Processors  
Enable annotation processing  
Make sure you have the Lombok plugin for IntelliJ installed!  
Preferences -> Plugins  
Search for "Lombok Plugin"  
Click Browse repositories...  
Choose Lombok Plugin  
Install  
Restart IntelliJ  