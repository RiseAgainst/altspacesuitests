package tests;

import base.Base;
import data.Credentials;
import data.Users;
import data_provider.DataProviders;
import org.testng.annotations.Test;
import pages.admin_panel.*;
import pages.admin_panel.record_details.EditContactDetails;
import pages.admin_panel.record_details.RecordDetails;
import pages.public_site.MainPage;
import pages.public_site.ThankYouPage;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static pages.public_site.MainPage.signUpForm;

@Features("Sign us tests")
public class SignUpTests extends Base {

    @Test
    @Stories("User can sign up")
    public void testCanSignUp() {
        new MainPage();
        signUpForm.fillSignUpForm(Users.testUserForSignUp);
        ThankYouPage thanksPage = signUpForm.submitForm();
        thanksPage.thankYouMessageIsVisible();
    }

    @Test(dependsOnMethods = "testCanSignUp")
    @Stories("Check signed up record details")
    public void testCheckSignUpDetails() {
        AdminPanel adminPanel = new Login().loginAs(Credentials.admin);
        adminPanel.checkSelectedProject("BUILDER");
        Inbox inbox = adminPanel.clickInbox();
        inbox.clickAll();
        Search search = inbox.openSearch();
        search.setEmail(Users.testUserForSignUp);
        search.submitSearch();
        RecordDetails details = search.openRecordDetails();
        details.checkUserData(Users.testUserForSignUp);
        //Tear down
        EditContactDetails editContactDetails = details.editContactDetails();
        editContactDetails.deleteEmail();
    }

    @Test(dataProvider = "invalidEmails", dataProviderClass = DataProviders.class)
    @Stories("Check email validation on the sign up form")
    public void testCheckEmailFieldValidation(String email) {
        new MainPage();
        signUpForm.fillEmail(email);
        signUpForm.submitForm();
        signUpForm.checkEmailErrorMessageIsVisible();
    }
}
