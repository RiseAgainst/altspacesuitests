package base;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import utils.DriverSetup;

@Listeners({ScreenshotListener.class})
public class Base {

    @BeforeClass
    public void setUp(){
        DriverSetup.setupDriver();
    }
}
