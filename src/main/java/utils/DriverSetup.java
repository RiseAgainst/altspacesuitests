package utils;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.ChromeDriverManager;

public class DriverSetup {

    public static void setupDriver(){
        Configuration.browser = "chrome";
        Configuration.timeout = 8000;
        Configuration.fastSetValue = false;
        Configuration.holdBrowserOpen = false;
        Configuration.baseUrl = "https://test.alternative-spaces.com/UI/" ;
        ChromeDriverManager.getInstance().setup();
    }

}
