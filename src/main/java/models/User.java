package models;


import lombok.Data;


@Data
public class User {

    private String firstName;
    private String lastName;
    private String zipCode;
    private String email;
    private String title;
    private String request;
    private String password;


    public User(String firstName, String lastName, String zipCode, String email, String title, String request) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.zipCode = zipCode;
        this.email = email;
        this.title = title;
        this.request = request;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }
}
