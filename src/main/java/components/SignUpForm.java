package components;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import models.User;
import org.openqa.selenium.By;
import pages.public_site.ThankYouPage;

import static com.codeborne.selenide.Selenide.$;

public class SignUpForm {
    private SelenideElement firstName = $(By.name("firstName"));
    private SelenideElement lastName = $(By.name("lastName"));
    private SelenideElement zipCode = $(By.name("zip"));
    private SelenideElement email = $(By.name("email"));
    private SelenideElement title = $(By.name("title"));
    private SelenideElement request = $(By.id("request"));
    private SelenideElement submit = $(By.xpath("//button[text() = 'Submit']"));

    //Errors
    private SelenideElement invalidEmailError = $(By.xpath("//*[@class = 'error-container' and text() = 'Email is invalid']"));

    public void fillFirstName(String firstNameValue) {
        firstName.val(firstNameValue);
    }

    public void fillLastName(String lastNameValue) {
        lastName.val(lastNameValue);
    }

    public void fillZipCode(String zipCodeValue) {
        zipCode.val(zipCodeValue);
    }

    public void fillEmail(String emailValue) {
        email.val(emailValue);
    }

    public void fillTitle(String titleValue) {
        title.val(titleValue);
    }

    public void fillRequest(String requestValue) {
        request.val(requestValue);
    }

    public ThankYouPage submitForm() {
        submit.click();
        return new ThankYouPage();
    }

    public void checkEmailErrorMessageIsVisible() {
        invalidEmailError.shouldBe(Condition.visible);
    }

    public SignUpForm fillSignUpForm(User user) {
        fillFirstName(user.getFirstName());
        fillLastName(user.getLastName());
        fillZipCode(user.getZipCode());
        fillEmail(user.getEmail());
        fillTitle(user.getTitle());
        fillRequest(user.getRequest());
        return this;
    }

}
