package pages.public_site;

import com.codeborne.selenide.Selenide;
import components.SignUpForm;

public class MainPage {

    public MainPage() {
        Selenide.open("https://site.alternative-spaces.com/");
    }

    public static SignUpForm signUpForm = new SignUpForm();
}
