package pages.public_site;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ThankYouPage {
    private SelenideElement thanksPageMessage = $(By.xpath("//*[text() = 'Requested page not found']"));

    public void thankYouMessageIsVisible() {
        thanksPageMessage.shouldBe(Condition.visible);
    }
}
