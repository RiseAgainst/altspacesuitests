package pages.admin_panel;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Inbox {
    private SelenideElement all = $(By.id("all_radio"));
    private SelenideElement search = $(By.id("searchLead"));


    public void clickAll() {
        all.click();
    }

    public Search openSearch() {
        search.click();
        return new Search();
    }
}
