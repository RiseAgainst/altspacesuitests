package pages.admin_panel;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import models.User;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Login {
    private static SelenideElement email = $(By.id("Login"));
    private static SelenideElement password = $(By.id("Password"));
    private static SelenideElement login = $(By.id("login-btn"));

    public Login() {
        Selenide.open("");
    }

    public static AdminPanel loginAs(User user) {
        email.val(user.getEmail());
        password.val(user.getPassword());
        login.click();
        return new AdminPanel();
    }

}
