package pages.admin_panel;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class AdminPanel {
    private SelenideElement projectSelector = $(By.id("projects-select"));
    private SelenideElement sales = $(By.xpath("//button[text() = 'sales']"));
    private SelenideElement inbox = $(By.xpath("//button[text() = 'inbox']"));

    public void checkSelectedProject(String expectedProject) {
        projectSelector.getSelectedOption().shouldHave(Condition.exactText(expectedProject));
    }

    public Inbox clickInbox() {
        clickSales();
        inbox.click();
        return new Inbox();
    }

    public void clickSales() {
        sales.click();
    }


}
