package pages.admin_panel;

import com.codeborne.selenide.SelenideElement;
import models.User;
import org.openqa.selenium.By;
import pages.admin_panel.record_details.RecordDetails;
import utils.WaitTool;

import static com.codeborne.selenide.Selenide.$;

public class Search {
    private SelenideElement email = $(By.id("search_email"));
    private SelenideElement submitSearch = $(By.id("searchButton"));
    private SelenideElement searchResult = $(By.className("leadRow"));

    public void setEmail(String emailValue) {
        email.val(emailValue);
    }

    public void setEmail(User user) {
        email.val(user.getEmail());
        WaitTool.waitFor(2000);
    }

    public void submitSearch() {
        submitSearch.click();
    }

    public RecordDetails openRecordDetails() {
        searchResult.click();
        return new RecordDetails();
    }


}
