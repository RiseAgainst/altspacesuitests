package pages.admin_panel.record_details;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import models.User;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class RecordDetails {
    private SelenideElement name = $(By.id("lead_name"));
    private SelenideElement email = $(By.xpath("//td[contains(text(), 'email')]"));
    private SelenideElement editLink = $(By.className("ContactDetailLeadEditLink"));

    public void checkName(String nameValue) {
        name.shouldHave(Condition.text(nameValue));
    }

    public void checkEmail(String emailValue) {
        email.shouldHave(Condition.text(emailValue));
    }

    public void checkUserData(User user) {
        checkName(user.getFullName());
        checkEmail(user.getEmail());
    }

    public EditContactDetails editContactDetails() {
        editLink.click();
        return new EditContactDetails();
    }
}
