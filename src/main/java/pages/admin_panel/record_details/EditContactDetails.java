package pages.admin_panel.record_details;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class EditContactDetails {
    private SelenideElement email = $(By.id("edit_lead_email"));
    private SelenideElement save = $(By.id("SaveDetailButton"));

    public void saveChanges() {
        save.click();
    }

    public void clearEmail() {
        email.clear();
    }

    public void deleteEmail() {
        clearEmail();
        saveChanges();
    }



}
